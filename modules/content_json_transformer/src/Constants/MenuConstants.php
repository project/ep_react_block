<?php

namespace Drupal\content_json_transformer\Constants;

/**
 * Class MenuConstants.
 */
class MenuConstants {

  const MIN_MENU_DEPTH = 0;
  const MAX_MENU_DEPTH = 5;
  const OUTPUT_VALUES = [
    'key' => 'key',
    'title' => 'title',
    'description' => 'description',
    'uri' => 'uri',
    'alias' => 'alias',
    'external' => 'external',
    'absolute' => 'absolute',
    'relative' => 'relative',
    'existing' => 'existing',
    'weight' => 'weight',
    'expanded' => 'expanded',
    'enabled' => 'enabled',
    'uuid' => 'uuid',
    'options' => 'options',
  ];

  /**
   * Get the constant min depth value of menu tree.
   */
  public static function getMinDepthValue() {
    return self::MIN_MENU_DEPTH;
  }

  /**
   * Get the constant max depth value of menu tree.
   */
  public static function getMaxDepthValue() {
    return self::MAX_MENU_DEPTH;
  }

  /**
   * Get the constant output value keys for a menu.
   */
  public static function getOutputValues() {
    return self::OUTPUT_VALUES;
  }

}
