<?php

namespace Drupal\content_json_transformer\services;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Menu\MenuLinkInterface;
use Drupal\Core\Path\AliasManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\system\Entity\Menu;
use Drupal\content_json_transformer\Constants\MenuConstants;

/**
 * Class MenuTransformerService.
 */
class MenuTransformerService {

  /**
   * A instance of the alias manager.
   *
   * @var \Drupal\Core\Path\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Path\AliasManagerInterface $alias_manager
   *   A manager to handle drupal link aliasing.
   */
  public function __construct(AliasManagerInterface $alias_manager) {
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Render\Element\ContainerInterface $container
   *   Drupal container holding all dependancies.
   *
   * @return static
   *   static container
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('path.alias_manager')
    );
  }

  /**
   * Grabs list of drupal menus and forms the necessary block selection array.
   *
   * @param array $field
   *   The title for the given form parameter.
   * @param string $configured_selection
   *   The configured selection.
   *
   * @return array
   *   The array needed by block api to create selection
   */
  public function createMenuOptionsFormArray(array $field, $configured_selection) {
    $menus = $this->getAllMenus();

    return [
      '#type' => 'select',
      '#title' => $field['title'],
      '#options' => $menus,
      '#default_value' => $configured_selection,
    ];
  }

  /**
   * Grabs list of drupal menus and forms the necessary block selection array.
   *
   * @return array
   *   The array needed by block api to create selection
   */
  private function getAllMenus() {
    $all_menus = Menu::loadMultiple();
    $menus = [];
    foreach ($all_menus as $id => $menu) {
      $menus[$id] = $menu->label();
    }
    asort($menus);

    return $menus;
  }

  /**
   * Grabs list of menu items from the menu name and returns an array.
   *
   * @param string $menu_name
   *   The name of menu we want a tree from.
   *
   * @return array
   *   The array needed by block api to create selection
   *
   * @throws \Exception
   *   Thrown cannot get internal path of inputted url.
   */
  public function getRenderableArrayOfMenuCategoriesFromName($menu_name) {
    if ($menu_name) {
      // Create the parameters.
      $parameters = new MenuTreeParameters();
      $parameters->onlyEnabledLinks();

      $parameters->setMaxDepth(MenuConstants::getMaxDepthValue());
      $parameters->setMinDepth(MenuConstants::getMinDepthValue());

      $menu_tree = \Drupal::menuTree();
      $tree = $menu_tree->load($menu_name, $parameters);

      if (empty($tree)) {
        return NULL;
      }

      $manipulators = [
        // Only show links that are accessible for the current user.
        ['callable' => 'menu.default_tree_manipulators:checkAccess'],
        // Use the default sorting of menu links.
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
      ];
      $tree = $menu_tree->transform($tree, $manipulators);

      $menu = $menu_tree->build($tree);

      // Return if the menu has no entries.
      if (empty($menu['#items'])) {
        // TODO: return empty items more gracefully...
        return NULL;
      }
      $final_menuitems = [];
      $this->getMenuItems($menu['#items'], $final_menuitems);

      return $final_menuitems;
    }

    return NULL;
  }

  /**
   * Grabs all attributes relating to each menu item.
   *
   * @param array $tree
   *   The name of menu we want a tree from.
   * @param array $items
   *   The accumulated value of all menu items with their respective attributes.
   *
   * @throws \Exception
   *   Thrown cannot get internal path of inputted url.
   */
  protected function getMenuItems(array $tree, array &$items = []) {
    $outputValues = MenuConstants::getOutputValues();
    $items = [];

    // Loop through the menu items.
    foreach ($tree as $item_value) {
      /* @var $org_link \Drupal\Core\Menu\MenuLinkInterface */
      $org_link = $item_value['original_link'];

      /* @var $url \Drupal\Core\Url */
      $url = $item_value['url'];

      $newValue = [];

      foreach ($outputValues as $valueKey) {
        if (!empty($valueKey)) {
          $this->getElementValue($newValue, $valueKey, $org_link, $url);
        }
      }

      if (!empty($item_value['below'])) {
        $newValue['below'] = [];
        $this->getMenuItems($item_value['below'], $newValue['below']);
      }

      $items[] = $newValue;
    }
  }

  /**
   * Grabs all attributes relating to the inputted menu item.
   *
   * @param array $returnArray
   *   The name of menu we want a tree from.
   * @param string $key
   *   The attribute key of current menu item.
   * @param \Drupal\Core\Menu\MenuLinkInterface $link
   *   The original link before any sort of aliasing.
   * @param \Drupal\Core\Url $url
   *   The relative url link for menu.
   *
   * @throws \Exception
   *   Thrown when cannot get the internal path of url.
   */
  private function getElementValue(array &$returnArray, $key, MenuLinkInterface $link, Url $url) {
    $external = $url->isExternal();
    $routed = $url->isRouted();
    $existing = TRUE;
    $value = NULL;

    if ($external || !$routed) {
      $uri = $url->getUri();
    }
    else {
      try {
        $uri = $url->getInternalPath();
      }
      catch (\UnexpectedValueException $e) {
        $uri = Url::fromUri($url->getUri())->toString();
        $existing = FALSE;
      }
    }

    switch ($key) {
      case 'key':
        $value = $link->getDerivativeId();
        if (empty($value)) {
          $value = $link->getBaseId();
        }
        break;

      case 'title':
        $value = $link->getTitle();
        break;

      case 'description':
        $value = $link->getDescription();
        break;

      case 'uri':
        $value = $uri;
        break;

      case 'alias':
        if ($routed) {
          $value = ltrim($this->aliasManager->getAliasByPath("/$uri"), '/');
        }
        break;

      case 'external':
        $value = $external;
        break;

      case 'absolute':
        if ($external) {
          $value = $uri;
        }
        elseif (!$routed) {
          $url->setAbsolute();
          $value = $url
            ->toString(TRUE)
            ->getGeneratedUrl();
        }
        else {
          $value = Url::fromUri('internal:/' . $uri, ['absolute' => TRUE])
            ->toString(TRUE)
            ->getGeneratedUrl();
        }
        break;

      case 'relative':
        if (!$external) {
          $value = Url::fromUri('internal:/' . $uri, ['absolute' => FALSE])
            ->toString(TRUE)
            ->getGeneratedUrl();
        }

        if (!$routed) {
          $url->setAbsolute(FALSE);
          $value = $url
            ->toString(TRUE)
            ->getGeneratedUrl();
        }

        if (!$existing) {
          $value = Url::fromUri($url->getUri())->toString();
        }
        break;

      case 'existing':
        $value = $existing;
        break;

      case 'weight':
        $value = $link->getWeight();
        break;

      case 'expanded':
        $value = $link->isExpanded();
        break;

      case 'enabled':
        $value = $link->isEnabled();
        break;

      case 'uuid':
        if (!$external && $routed) {
          $params = Url::fromUri('internal:/' . $uri)->getRouteParameters();
          $entity_type = key($params);
          if (!empty($entity_type)) {
            $entity = \Drupal::entityTypeManager()
              ->getStorage($entity_type)
              ->load($params[$entity_type]);
            $value = $entity->uuid();
          }
        }
        break;

      case 'options':
        $value = $link->getOptions();
        break;
    }

    $returnArray[$key] = $value;
  }

}
