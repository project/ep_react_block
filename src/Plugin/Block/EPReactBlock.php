<?php

namespace Drupal\pdb_ep_react\Plugin\Block;

use Drupal\pdb\Plugin\Block\PdbBlock;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pdb_ep_react\content\ContentFieldResolver;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\content_json_transformer\services\MenuTransformerService;

/**
 * Exposes a React component as a block.
 *
 * @Block(
 *   id = "ep_react_component",
 *   admin_label = @Translation("EP Reference component"),
 *   deriver = "\Drupal\pdb_ep_react\Plugin\Derivative\EPReactBlockDeriver"
 * )
 */
class EPReactBlock extends PdbBlock {

  private $rand;

  protected $menuTransformerService;

  /**
   * PdbBlock constructor.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\content_json_transformer\services\MenuTransformerService $menuTransformerService
   *   Service that fetches drupal menu information.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MenuTransformerService $menuTransformerService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuTransformerService = $menuTransformerService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('content_json_transformer.menus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    $data_values = [];

    if (array_key_exists('block_data_output', $config)) {
      $data_values = $this->serializeBlockOutputValues($config['block_data_output']);
    }

    $content_config = $this->getContentConfiguration();
    if (isset($content_config)) {
      $resolver = new ContentFieldResolver();
      $content_fields = $resolver->get_content_fields($content_config);
      $data_values = array_merge($data_values, $content_fields);
    }

    $build = parent::build();

    $machine_name = $this->getComponentInfo()['machine_name'];
    $markup = $this->buildMarkup($machine_name, $data_values);

    $build['#allowed_tags'] = [$machine_name];
    $build['#markup'] = $markup;

    // don't cache plz.
    $build['#cache'] = ['max-age' => 0];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $form_keys = $this->getConfigKeys();

    foreach ($form_keys as $key) {
      $this->configuration['block_data_output'][$key] = $values[$key];
    }
  }

  /**
   * Builds markup to be returned to block form api.
   *
   * @param string $machine_name
   *   The machine name of the block.
   * @param array $data_values
   *   The data values to be placed in html 5 data attribute.
   *
   * @return markup
   *   returns the markup.
   */
  private function buildMarkup($machine_name, array $data_values) {
    $markup = '<' . $machine_name . ' id="' . $machine_name . $this->rand . '" ';

    foreach ($data_values as $key => $value) {
      $markup .= 'data-' . str_replace('_', '-', $key) . '="' . $value . '" ';
    }
    $markup .= '></' . $machine_name . '>';
    return $markup;
  }

  /**
   * Get the content configuration.
   */
  private function getContentConfiguration() {
    $info = $this->getComponentInfo();
    if (array_key_exists('content_configuration', $info)) {
      return $info['content_configuration'];
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function attachSettings(array $component) {
    // This function gets called before the build function runs
    // we have to assign the random value now.
    $this->rand = "-" . $this->getGuid();
    $machine_name = $component['machine_name'];
    $attached = [];

    if (array_key_exists('path', $component)) {
      $attached['drupalSettings']['react-apps'][$machine_name . $this->rand]['uri'] = '/' . $component['path'];
    }
    else {
      $attached['drupalSettings']['react-apps'][$machine_name . $this->rand]['uri'] = '/';
    }

    return $attached;
  }

  /**
   * {@inheritdoc}
   */
  public function attachLibraries(array $component) {
    $parent_libraries = parent::attachLibraries($component);

    $libraries = [
      'library' => $parent_libraries,
    ];

    return $libraries;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form_config = $this->getFormConfiguration();
    if (isset($form_config)) {
      foreach ($form_config as $field) {
        $config_title = $this->makeConfigName($field['title'], $field['type']);

        if ($field['type'] == 'menu') {
          $form[$config_title] = $this->menuTransformerService->createMenuOptionsFormArray($field, $config['block_data_output'][$config_title]);
        }
        else {
          $form[$config_title] = [
            '#title' => $this->t($field['title']),
            '#type' => $field['type'],
            '#description' => $this->t($field['description']),
            '#default_value' => isset($config['block_data_output'][$config_title]) ? $config['block_data_output'][$config_title] : '',
          ];
        }
      }
    }

    return $form;
  }

  /**
   * Function that gets list of menu items from name.
   *
   * @param string $categoryName
   *   Category name used to get menu items.
   *
   * @return array
   *   returns the tree of menu items with attributes.
   *
   * @throws \Exception
   *   Thrown cannot get internal path of inputted url.
   */
  private function getAllMenuCategories($categoryName) {
    $menuItem = $this->menuTransformerService->getRenderableArrayOfMenuCategoriesFromName($categoryName);
    return $menuItem;
  }

  /**
   * Checks whether the given string has a menu prefix.
   *
   * @param string $key
   *   The key from block configuration.
   *
   * @return bool
   *   whether it has menu in the entered string.
   */
  private function isMenuSelection($key) {
    return explode("_", $key)[0] == 'menu';
  }

  /**
   * Gets the block form configuration.
   */
  private function getFormConfiguration() {
    $info = $this->getComponentInfo();

    if (array_key_exists('form_configuration', $info)) {
      return $info['form_configuration'];
    }

    return NULL;
  }

  /**
   * Strips the prefix from key string.
   *
   * @param string $block_config_key
   *   The block config key to strip prefix from.
   *
   * @return string
   *   the substring key value.
   */
  private function stripTypeFromKey($block_config_key) {
    return explode('_', $block_config_key, 2)[1];
  }

  /**
   * Function will base64 encode all values to be outputted to data attribute.
   *
   * @param array $block_config
   *   The unserialized configuration of the given block.
   *
   * @return array
   *   the serialized array to be outputted.
   *
   * @throws /exception
   *   Exception thrown while grabbing menu items.
   */
  private function serializeBlockOutputValues(array $block_config = NULL) {
    $data_values = [];

    foreach ($block_config as $block_layout_config_key => $block_layout_config_value) {
      $block_data_output_key = $this->stripTypeFromKey($block_layout_config_key);

      if ($this->isMenuSelection($block_layout_config_key)) {
        $menuItems = $this->getAllMenuCategories($block_layout_config_value);
        $menuItemsJS = base64_encode(json_encode($menuItems));

        $data_values[$block_data_output_key] = $menuItemsJS;
      }
      else {
        $data_values[$block_data_output_key] = base64_encode($block_layout_config_value);
      }
    }

    return $data_values;
  }

  /**
   * Gets the configuration keys and appends type.
   */
  private function getConfigKeys() {
    $form_config = $this->getFormConfiguration();
    $result = [];
    if (isset($form_config)) {
      foreach ($form_config as $field) {
        $result[] = $this->makeConfigName($field['title'], $field['type']);
      }
    }
    return $result;
  }

  /**
   * Appends type and title from yml file to create unique key.
   *
   * @param string $title
   *   The title of configuration from yml file.
   * @param string $type
   *   The type of configuration from yml file.
   *
   * @return string
   *   the appended string of title and type.
   */
  private function makeConfigName($title, $type) {
    $underscored_title = str_replace(' ', '_', strtolower($title));
    return $type . '_' . $underscored_title;
  }

  /**
   * Creates a GUID String.
   *
   * @return string
   *   the globally unique identifier.
   */
  private function getGuid() {
    if (function_exists('com_create_guid') === TRUE) {
      return trim(com_create_guid(), '{}');
    }
    $data = openssl_random_pseudo_bytes(16);
    // Set version to 0100.
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40);
    // Set bits 6-7 to 10.
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80);
    return vsprintf('%s%s-%s-%s-%s-%s%s%s', str_split(bin2hex($data), 4));
  }

}
