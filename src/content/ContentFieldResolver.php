<?php

namespace Drupal\pdb_ep_react\content;

/**
 * ContentFieldResolver resolves a configurable list of field names
 * from the active content entity if we are accessing a content entity page.
 */
class ContentFieldResolver {

  /**
   * Given a map of content types to lists of fields, resolves those fields
   * and returns a map of [ fieldname => value ].
   */
  public function get_content_fields($config) {
    $node = $this->get_node_for_request();
    if (isset($node)) {
      $node_type = $node->getType();

      foreach ($config as $content_config) {
        if ($content_config['content_type'] === $node_type) {
          $fields = $content_config['fields'];
          $values = [];
          foreach ($fields as $field) {
            if ($node->hasField($field)) {
              $values[$field] = $node->$field->value;
            }
          }
          return $values;
        }
      }
    }
    return [];
  }

  /**
   * Gets the active content node for the request
   * if any.
   */
  private function get_node_for_request() {
    $route_match = \Drupal::routeMatch();
    if ($route_match->getRouteName() === 'entity.node.canonical') {
      return $route_match->getParameter('node');
    }
    return NULL;
  }

}
